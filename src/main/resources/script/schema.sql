CREATE TABLE products (
                         id serial PRIMARY KEY ,
                         name varchar(50) not null,
                         price float not null
);

CREATE TABLE customer (
                          id serial PRIMARY KEY ,
                          name varchar(50) not null,
                          address varchar(100) not null,
                          phone varchar(10) not null
);

CREATE TABLE invoice (
    id serial PRIMARY KEY ,
    date timestamp default current_timestamp,
    cusId int REFERENCES customer(id)
);

CREATE TABLE invoice_detail(
    id serial PRIMARY KEY ,
    invoice_id INT REFERENCES invoice(id) ON DELETE CASCADE ON UPDATE CASCADE ,
    product_id INT REFERENCES products(id) ON DELETE CASCADE ON UPDATE CASCADE
)