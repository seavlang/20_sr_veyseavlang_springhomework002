package com.example.spring_20_sr_veyseavlang_mybatis.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {

    private Timestamp date;
    private Integer cusid;
    private List<Integer> productId;
}
