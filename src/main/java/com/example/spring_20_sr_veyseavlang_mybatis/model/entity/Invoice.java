package com.example.spring_20_sr_veyseavlang_mybatis.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Invoice {
    private Integer id;
    private Timestamp date;
    private Customer customer;
    private List<Product> products;

}
