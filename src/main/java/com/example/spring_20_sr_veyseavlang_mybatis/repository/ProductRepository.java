package com.example.spring_20_sr_veyseavlang_mybatis.repository;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductRepository {
    @Select("SELECT * FROM products")
    List<Product> findAllProduct();

    @Select("SELECT * FROM products WHERE id = #{id}")
    Product findProductByid(@Param("id") Integer ProId);

    @Delete("DELETE FROM products WHERE id = #{id}")
    Boolean deleteProductById(@Param("id") Integer ProId);

    @Select("INSERT INTO products(name,price) VALUES (#{request.name},#{request.price}) RETURNING id")
    Integer addNewProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE products SET name = #{update.name}, price = #{update.price} WHERE id = #{ProId} RETURNING id")
    Integer updateProductById(@Param("update") ProductRequest productRequest, Integer ProId);

    @Select("SELECT p.id, p.name, p.price FROM products p " +
            "INNER JOIN invoice_detail i ON p.id = i.product_id WHERE i.invoice_id = #{invoiceId}")
    List<Product> getProductByInvoiceId(Integer invoiceId);

}
