package com.example.spring_20_sr_veyseavlang_mybatis.repository;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Invoice;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.InvoiceRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice")
    @Results(
        id="InvoiceMapper",
        value={ @Result(property = "id",column="id"),
                @Result(property = "customer" ,column="cusid",
                one = @One(select = "com.example.spring_20_sr_veyseavlang_mybatis.repository.CustomerRepository.findCustomerById")),
                @Result(property = "products" ,column = "id",
                many = @Many(select = "com.example.spring_20_sr_veyseavlang_mybatis.repository.ProductRepository.getProductByInvoiceId")),

    })
    List<Invoice> findAllInvoice();


    @Select("SELECT * FROM invoice WHERE id = #{id}")
    @ResultMap("InvoiceMapper")
    Invoice findInvoiceById(Integer invoiceId);

    @Delete("DELETE FROM invoice WHERE id = #{id}")
    @ResultMap("InvoiceMapper")
    Boolean deleteInvoiceById(@Param("id") Integer invoiceId);

    @Select("INSERT INTO invoice(date,cusid) VALUES (#{request.date},#{request.cusid}) RETURNING id")
    Integer addNewInvoice(@Param("request")InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoice_detail(invoice_id,product_id) VALUES (#{invoiceId},#{ProId})")
    Integer saveProductByInvoiceId(Integer invoiceId,Integer ProId);

    @Select("UPDATE invoice SET date = #{update.date}, cusid = #{update.cusid} WHERE id = #{invoiceId} RETURNING id")
    Integer updateInvoiceById(@Param("update") InvoiceRequest invoiceRequest, Integer invoiceId);

    @Select("DELETE FROM products WHERE id = #{id}")
    @ResultMap("InvoiceMapper")
    Integer DeleteProductByInvoiceId(@Param("id") Integer invoiceId);

    @Select("INSERT INTO invoice_detail(invoice_id,product_id) VALUES (#{invoiceId},#{ProId})")
    Integer updateProductByInvoiceId(Integer invoiceId,Integer ProId);

    }
