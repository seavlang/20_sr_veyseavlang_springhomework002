package com.example.spring_20_sr_veyseavlang_mybatis.repository;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Customer;
import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.CustomerRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CustomerRepository {
    @Select("SELECT * FROM customer")
    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customer WHERE id = #{id}")
    Customer findCustomerById(@Param("id") Integer CusId);

    @Delete("DELETE FROM customer WHERE id = #{id}")
    Boolean deleteCustomerById(@Param("id") Integer CusId);

    @Select("INSERT INTO customer(name,address,phone) VALUES (#{request.name},#{request.address},#{request.phone}) RETURNING id")
    Integer addNewCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customer SET name = #{update.name}, address = #{update.address}, phone = #{update.phone}  WHERE id = #{CusId} RETURNING id")
    Integer updateCustomerById(@Param("update") CustomerRequest customerRequest, Integer CusId);
}
