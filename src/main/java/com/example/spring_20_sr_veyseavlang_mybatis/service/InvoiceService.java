package com.example.spring_20_sr_veyseavlang_mybatis.service;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Invoice;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.InvoiceRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();
    Invoice getInvoiceById(Integer invoiceId);
    Boolean deleteInvoiceById(Integer invoiceId);
    Integer addNewInvoice(InvoiceRequest invoiceRequest);
    Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId);
}
