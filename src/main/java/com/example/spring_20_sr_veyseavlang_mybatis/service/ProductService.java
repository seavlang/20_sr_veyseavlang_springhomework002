package com.example.spring_20_sr_veyseavlang_mybatis.service;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(Integer ProId);
    Boolean deleteProductById(Integer ProId);
    Integer addNewProduct(ProductRequest productRequest);
    Integer updateProductById(ProductRequest productRequest, Integer ProId);
}
