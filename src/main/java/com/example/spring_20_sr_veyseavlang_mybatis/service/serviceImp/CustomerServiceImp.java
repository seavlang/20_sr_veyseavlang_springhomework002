package com.example.spring_20_sr_veyseavlang_mybatis.service.serviceImp;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Customer;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.CustomerRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.repository.CustomerRepository;
import com.example.spring_20_sr_veyseavlang_mybatis.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    public final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer CusId) {
        return customerRepository.findCustomerById(CusId);
    }

    @Override
    public Boolean deleteCustomerByid(Integer CusId) {
        return customerRepository.deleteCustomerById(CusId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.addNewCustomer(customerRequest);
    }

    @Override
    public Integer updateCustomerById(CustomerRequest customerRequest, Integer CusId) {
        return customerRepository.updateCustomerById(customerRequest,CusId);
    }
}
