package com.example.spring_20_sr_veyseavlang_mybatis.service.serviceImp;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.repository.ProductRepository;
import com.example.spring_20_sr_veyseavlang_mybatis.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer ProId) {
        return productRepository.findProductByid(ProId);
    }

    @Override
    public Boolean deleteProductById(Integer ProId) {
        return productRepository.deleteProductById(ProId);
    }


    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer proId = productRepository.addNewProduct(productRequest);
        return proId;
    }

    @Override
    public Integer updateProductById(ProductRequest productRequest, Integer ProId) {
        return productRepository.updateProductById(productRequest,ProId);
    }

}
