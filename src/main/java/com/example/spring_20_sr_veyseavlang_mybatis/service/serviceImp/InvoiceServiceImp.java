package com.example.spring_20_sr_veyseavlang_mybatis.service.serviceImp;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Invoice;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.InvoiceRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.repository.InvoiceRepository;
import com.example.spring_20_sr_veyseavlang_mybatis.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {
    public  final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.findInvoiceById(invoiceId);
    }

    @Override
    public Boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {

        Integer newInvoiceId = invoiceRepository.addNewInvoice(invoiceRequest);
        for (Integer productId:invoiceRequest.getProductId()) {
            invoiceRepository.saveProductByInvoiceId(newInvoiceId,productId);
        }
        return newInvoiceId;
    }

    @Override
    public Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId) {
        Integer newInvoiceId = invoiceRepository.updateInvoiceById(invoiceRequest,invoiceId);
        for (Integer id :invoiceRequest.getProductId()) {
            invoiceRepository.DeleteProductByInvoiceId(id);
            invoiceRepository.updateProductByInvoiceId(invoiceId,id);
        }
        return newInvoiceId;
    }
}
