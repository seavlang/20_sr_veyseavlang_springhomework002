package com.example.spring_20_sr_veyseavlang_mybatis.service;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Customer;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.CustomerRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer getCustomerById(Integer CusId);
    Boolean deleteCustomerByid(Integer CusId);

    Integer addNewCustomer(CustomerRequest customerRequest);
    Integer updateCustomerById(CustomerRequest customerRequest, Integer CusId);
}
