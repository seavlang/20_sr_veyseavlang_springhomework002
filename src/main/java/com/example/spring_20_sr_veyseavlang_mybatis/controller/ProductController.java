package com.example.spring_20_sr_veyseavlang_mybatis.controller;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.ProductResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    public final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    @Operation(summary = "Get all Products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProducts(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Get Products Successfully!")
                .payload(productService.getAllProducts())
                .status(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Product By Id")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer ProId){
        if(productService.getProductById(ProId) != null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Get Product Successfully!")
                    .payload(productService.getProductById(ProId))
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Can not Get Product!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Product By Id")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer ProId){
        ProductResponse<String> response = null;
        if(productService.deleteProductById(ProId)==true) {
           response = ProductResponse.<String>builder()
                    .message("Delete Product Successfully!")
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = ProductResponse.<String>builder()
                    .message("Can not Delete Product!!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @PostMapping("/save")
    @Operation(summary = "Add new Product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer newId = productService.addNewProduct(productRequest);
        ProductResponse<Product> response = null;
        if(newId !=null){
            response = ProductResponse.<Product>builder()
                    .message("Add New Product Successfully!")
                    .status(HttpStatus.OK)
                    .payload(productService.getProductById(newId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update Product By Id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(@RequestBody ProductRequest productRequest, @PathVariable("id") Integer ProId){
        ProductResponse<Product> response = null;
        Integer updateId = productService.updateProductById(productRequest,ProId);
        if(updateId !=null){
            response = ProductResponse.<Product>builder()
                    .message("Update Product Successfully!")
                    .status(HttpStatus.OK)
                    .payload(productService.getProductById(updateId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = ProductResponse.<Product>builder()
                    .message("Can not Update Product!!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }
}
