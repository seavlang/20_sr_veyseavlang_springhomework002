package com.example.spring_20_sr_veyseavlang_mybatis.controller;


import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Customer;
import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Invoice;
import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.InvoiceRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.CustomerResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.InvoiceResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.ProductResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {
    public final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/invoices")
    @Operation(summary = "Get all Invoices")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .message("Get Invoices Successfully!")
                .payload(invoiceService.getAllInvoice())
                .status(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Invoice By Id")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        if(invoiceService.getInvoiceById(invoiceId) !=null) {
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Get Invoices Successfully!")
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Can Not Get Invoices!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Invoice By Id")
    public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<String> response = null;
        if(invoiceService.deleteInvoiceById(invoiceId)==true) {
            response = InvoiceResponse.<String>builder()
                    .message("Delete Invoice Successfully!")
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = InvoiceResponse.<String>builder()
                    .message("Can not Delete Invoice!!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }
    @PostMapping("/save")
    @Operation(summary = "Add new Invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        Integer newId = invoiceService.addNewInvoice(invoiceRequest);
        InvoiceResponse<Invoice> response = null;
        if(newId !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Add New Invoice Successfully!")
                    .status(HttpStatus.OK)
                    .payload(invoiceService.getInvoiceById(newId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
    @PutMapping("/{id}")
    @Operation(summary = "Update Product By Id")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoiceById(@RequestBody InvoiceRequest invoiceRequest, @PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        Integer updateId = invoiceService.updateInvoiceById(invoiceRequest,invoiceId);
        if(updateId !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Update Invoice Successfully!")
                    .status(HttpStatus.OK)
                    .payload(invoiceService.getInvoiceById(updateId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = InvoiceResponse.<Invoice>builder()
                    .message("Can not Update Invoice!!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

}
