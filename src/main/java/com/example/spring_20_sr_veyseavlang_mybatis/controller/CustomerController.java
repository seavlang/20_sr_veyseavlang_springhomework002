package com.example.spring_20_sr_veyseavlang_mybatis.controller;

import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Customer;
import com.example.spring_20_sr_veyseavlang_mybatis.model.entity.Product;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.CustomerRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.request.ProductRequest;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.CustomerResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.model.response.ProductResponse;
import com.example.spring_20_sr_veyseavlang_mybatis.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    public final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    @Operation(summary = "Get all Customers")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomers(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Get Customers Successfully!")
                .payload(customerService.getAllCustomer())
                .status(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Customer By Id")
    public ResponseEntity<CustomerResponse<Customer>> getProductById(@PathVariable("id") Integer CusId){
        if(customerService.getCustomerById(CusId) != null) {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Get Customer Successfully!")
                    .payload(customerService.getCustomerById(CusId))
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Can not Get Customer!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Customer By Id")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer CusId){
        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerByid(CusId)==true) {
            response = CustomerResponse.<String>builder()
                    .message("Delete Customer Successfully!")
                    .status(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = CustomerResponse.<String>builder()
                    .message("Can not Delete Customer!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @PostMapping("/save")
    @Operation(summary = "Add new Customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer newId = customerService.addNewCustomer(customerRequest);
        CustomerResponse<Customer> response = null;
        if(newId !=null){
            response = CustomerResponse.<Customer>builder()
                    .message("Add New Product Successfully!")
                    .status(HttpStatus.OK)
                    .payload(customerService.getCustomerById(newId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update Customer By Id")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer CusId){
        CustomerResponse<Customer> response = null;
        Integer updateId = customerService.updateCustomerById(customerRequest,CusId);
        if(updateId !=null){
            response = CustomerResponse.<Customer>builder()
                    .message("Update Customer Successfully!")
                    .status(HttpStatus.OK)
                    .payload(customerService.getCustomerById(updateId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            response = CustomerResponse.<Customer>builder()
                    .message("Can not Update Customer!!")
                    .status(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

}
